# fakesh

## _Shell logging wrapper that can interpret "`#!`" lines_

## Highlights

* [Install](#install)
* [Configuration](#configuration)
  * [Timestamps](#timestamps)
* [Log converter](#log-converter)
* [Applet-biased busybox](#applet-biased-busybox)
  * [setup-bbox script](#setup-bbox-script)
* [License & copyright](#copyright)


## Install

*Note*: please review the [changelog](CHANGELOG.md) if upgrading.

Copy `fakesh.sh` somewhere in `$PATH` (possibly renamed as `sh`, but be careful) and/or configure it as a shell for other applications that support it:
- [Kakoune](https://github.com/mawww/kakoune/): `KAKOUNE_POSIX_SHELL=.../fakesh.sh kak ...`
- vim: `set shell=`

### Customize

Edit the shebang line of `fakesh.sh` to wrap a shell different from `/bin/sh`. In particular, an [applet-biased "**`/.../busybox sh`**"](#applet-biased-busybox) will yield very good results with applications that use the shell extensively (Kakoune) as it replaces many common utilities with fast-starting builtins. This repo includes a script to [set up such a busybox](#setup-bbox-script) version and have `fakesh` use it. Apps can also use busybox directly without going through `fakesh`.

## Operation

If enabled, `fakesh` logs invocation start / end [timestamps](#timestamps) and arguments into `/tmp/fakesh.log` (or the configured alternative), and increments a counter in `/tmp/fakesh_cnt.log`. Use the [log converter](#log-converter) tool, or read the specs below to process these logs; a use-case is profiling "shell-hungry" apps like Kakoune (that's how `fakesh` was born). Then:
- if given a `-c <command>` with a shebang ( e.g. `#!/usr/bin/env perl ...`) line, and `F8KSH_FAKESH_PARSE_SHEBANG = true`, it will call the specified interpreter (which need **not be an absolute** filename) with the rest of the body passed as a temporary file (followed by arguments `$3` and on, as customary with all shells &mdash; `$2` is a "self-rename" argument). The net effect is the same as **running the script from an actual file** from the command line, except you can use "`#!lua`" (or anything that's in `$PATH`) directly.
- if given a `-c <command-string>` without a shebang (and optional subsequent arguments) it calls the underlying shell's `eval` &mdash; minimal cost, **no-fork** case
- in any other form it doesn't know how to handle, `fakesh` will defer to (`exec`) the shell that is specified in `fakesh`'s own shebang line

If you want to monitor `sh` calls for an application, run `tail -f /tmp/fakesh.log` from a separate window. Or analyze the logs once to app quits. Use `F8KSH_FAKESH_DATE` as explained below to get precise timestamps.

## Configuration

- `F8KSH_FAKESH_DEBUG [= false]`: log invocation arguments and start / end timestamps (see [below](#timestamps)) to `/tmp/fakesh.log`
- `F8KSH_FAKESH_COUNT [= false]`: count invocations in `/tmp/fakesh_cnt.log`; much less resource-intensive than turning on `_DEBUG`
- `F8KSH_FAKESH_LOGDIR [= ${TMPDIR:-/tmp}]`: log file location
- `F8KSH_FAKESH_RC`: source this file in every shell; e.g. set the other config variables. If not empty, and `~/.config/f8ksh/fakesh.rc` exists, source it.
- `F8KSH_FAKESH_EVAL`: prologue to execute when starting every shell (after loading `_CFG` file, if any); e.g. `set -x` to turn on tracing

### Timestamps

`F8KSH_FAKESH_DATE` should be a command to produce timestamps. By default, `fakesh` automatically selects a command that outputs "nanoseconds since Epoch" (1) as fast as possible, and (2) with as much precision as possible, *in this order*. In particular, the default (`debian-static`) version of the [setup-busybox](#setup-bbox-script) script generates **imprecise second-level timestamps** only, so you should either **use --busybox=make**, or  *set `..._DATE`* to an appropriate command:
- run `util/fifo-srv.sh` and set `..._DATE` as instructed (e.g. `head -1 /tmp/nsecsrv-fifo`). This will generate fast, precise timestamps with any busybox shell, but can be fragile / annoying to clean up. The options below (except for `=make`) all add overhead and perturb the measured scripts (which may not matter &mdash; are you investigating a few slow scripts, or thousands of short-lived shell calls?)
- with GNU `date` (default on Linux, `gdate` on BSDs), use `/bin/date +%s%N` (*including the path*) to get nanosecond-level integer timestamps
- compile [`udate.c`](util/udate.c) (like "date", but only handles `+FMT`; it supports `+%s%N`, which is the default with no arguments). Set `..._DATE=/path/to/udate`.
- set `..._DATE` to shell code that either
    - outputs a timestamp, or
    - sets the implicit variable `now` to a timestamp (for this, the code must start with `now=`; it can span multiple commands)
- again, use `--busybox=make` and unset `..._DATE`. This will take a while and requires developer tools to be installed.

It's also possible (but not so useful for accounting purposes) to use human-style timestamps, e.g. `date -I[ns]`. You may want to use `-u` or prepend `TZ=UTC0` to get UTC timestamps regardless of your timezone (this is not necessary for epoch-style timestamps).

## Log files

Note: you can [skip](#log-converter) this section, unless you need to write a custom `fakesh.log` parser.

`/tmp/fakesh_cnt.log` is a single integer. `/tmp/fakesh.log` is a log that is also a shell-script. Each line is a command a shell command (`log`) with shell-quoted arguments (`cat <<'EOF'` quoting is used because it's the fastest to produce; it makes the log extremely verbose). For example, `F8KSH_FAKESH_DEBUG=true F8KSH_FAKESH_DATE='/bin/date +%s%N' ./fakesh.sh -c ls` will produce a (nanosecond-level) [time-stamped](#timestamps) log.
```
log pid="819031" ppid="805475" type="start" time="1625301737796806791" \
   arg_1="$(cat <<'e108dd7bb5d7af773a2_EOF'
-c
e108dd7bb5d7af773a2_EOF
)" arg_2="$(cat <<'e108dd7bb5d7af773a2_EOF'
ls
e108dd7bb5d7af773a2_EOF
)";
log pid="819031" ppid="805475" time="1625301737799356271" desc="eval" type="end";
```

If you pre-define `log()` and execute the script, it should re-generate the log itself (this is what the provided [converter](#log-converter) does). For example, with GNU `coreutils` `printf`, convert the log to `bash`-style dollar-quoting (`$'\n'` for newlines):
```
# log() { :; } # just ignore
log() { /usr/bin/printf '%q ' log "$@" "# last=$#"; echo; }
busybox sh -x /tmp/fakesh.log >/tmp/fakesh.dollarq.log
```

### Log converter

[`fakesh-log-conv`](./fakesh-log-conv) reads a `fakesh` log from stdin and converts it to a different format. By default, it converts to the more compact single-quoted format ("`my 'str' here"` -> `'my '\''str'\'' here'`).

Use `--logfunc=` to specify a different converter. The most useful: `--logfunc=2sql` converts to SQL insert commands (plus code to create tables and views). The generated SQL can be passed on to "`| sqlite3 fakesh.sqlite3`"; in particular, take a look at the views `run` and `cmd_stat`:
```
-- F8KSH_FAKESH_DATE='/bin/date +%s%N' F8KSH_FAKESH_DEBUG=true COMMAND
-- fakesh-log-conv --logfunc=2sql < fakesh.log | sqlite3 fs.sqlite3
-- sqlite3 fs.sqlite3
.mode line
select * from cmd_stat order by tdelta_total desc limit 3;
-- or cnt, tdelta_avg, tdelta_max
-- times in nanoseconds (/ 1000000 -> msec)
```

Additional options:
- `--pp-*='cmd ...'` (e.g. `--pp-time)`: preprocess the corresponding field using a command
- `--eval='...'`: `eval` the specified code before doing anything

Like `fakesh`, the converter will run considerably **faster under busybox** (`busybox static-sh fakesh-log-conv`, or modify its shebang permanently).

## Applet-biased busybox

`busybox` can provide `sed` / `grep` / `awk`, `mkdir`, `head / tail` and others. To call busybox versions ("applets") of standard utilities, you call "`busybox ` *applet-name*", or set up a symlink from busybox to *applet-name* in `$PATH`. However, there is a busybox build configuration (`FEATURE_SH_STANDALONE + ..._PREFER_APPLETS`) that runs internal applets for any known commands, without any `$PATH` lookup. This speeds up most scripts, as frequent calls to external utilities are replaced with busybox builtins.[^1]

To install such an "applet-biased" version, see the [busybox setup script](#setup-bbox-script) below. On Debian / Ubuntu, you could install the `busybox-static` package, and either use `/bin/static-sh` (Ubuntu) or a `sh` symlink to busybox; however the script can produce many more configurations.

When switching apps to `busybox`, note that some configurations, unlike shebangs, require a single-word shell, not a multi-argument command line. E.g. to switch `kak` permanently to `busybox`, use `/bin/static-sh` on Debian, or a `sh` created by the script below; either way, set `KAKOUNE_POSIX_SHELL` to an absolute location.

### `setup-bbox` script

[`setup-bbox`](./setup-bbox) can set up a directory containing an **"applet-biased" busybox and a `fakesh`** configured to use it. By default (`setup-bbox --help`) it selects a directory in `tmpfs` fast storage (`/run`) and goes through these steps:
1. locate an existing busybox version (required; install one beforehand, download [from `busybox.net`](https://www.busybox.net/downloads/binaries/1.31.0-defconfig-multiarch-musl/), or compile, &mdash; then pass `--busybox0=`)
1. if that version is "applet-biased" (e.g. on Debian / Ubuntu), use it; if not, download a Debian `busybox-static` package for your system architecture and extract `busybox` from it. If `--busybox=` given (see `--help` for defaults):
    - `=make` will compile busybox from --busybox-src repository (filename or URL), --busybox-branch (or defaults if unspecified). Use `--[no-]busybox-config-static` to control whether the built binary will be static (default) or dynamically linked, and `...config-nofork`whether to enable a further aggressive `busybox` optimization for applets that support it.
    - `=alpine` will download an Alpine Linux `busybox-static` `.apk`, while `=debian[-static]` will download a `.deb`; both of these package formats can be handled with generic tools. Note that these versions, while fast to install, lead to imprecise [timestamps](#timestamps) in the default configuration, and you will need to **configure `F8KSH_FAKESH_DATE`**.
1. if the selected busybox still requires applet links, create them (unless `--busybox-links=false`) in the target dir
1. also drop a `fakesh.sh` copy with a modified shebang line
1. copy a few other files ([`udate`](util/udate.c) if compiled, dash) so they can also run from fast storage
1. print the path to the configured target dir

## Copyright

`Alin Mr. <almr.oss@outlook.com>` / MIT license

---

Back to
- [content highlights](#highlights)
- [configuration](#configuration)
- [busybox notes](#applet-biased-busybox)

---

[^1]: You might encounter problems with code that assumes `awk` / `sed` / `grep` / whatever are their GNU versions. To force `busybox sh` to use the "real" utility, provide a full path &mdash; directly (e.g. `/bin/readlink`), or by calling via `/usr/bin/env` (not bare `env`, which is again a builtin).

