#!/usr/bin/env bash
set -u; set -e

# /my/busybox sh fifo-srv.sh  # runs in any POSIX shell
## no distro binary, other than bash, has either $EPOCH* or nanodate
## bash is easiest to locate and has $EPOCH*

## $EPOCHREALTIME support: busybox (CONFIG_ASH_BASH_COMPAT, e.g. debian), bash
## fast 'date +%s%N': busybox (FEATURE_SH_STANDALONE + CONFIG_FEATURE_DATE_NANO)
# /my/busybox sh -c 'echo $EPOCHREALTIME; PATH= date +%s%N'  # test

## With no args, nanodate server; or specify command:
# fifo-srv '' 'echo /tmp/*'
fifo=${1:-/tmp/nsecsrv-fifo}
cmd=${2:-}

__set_defaults() {
  if test -z "$cmd"; then  # configure as time server
    if test -n "${EPOCHREALTIME:-}"; then
      cmd='now=$EPOCHREALTIME; echo ${now%.*}${now#*.}000'
    elif test -n "$(x=$(PATH= date +%s%N || :); echo "${x##*N}")"; then
      cmd='date +%s%N'
    else
      date=$(exec 2>/dev/null; which gdate || which date)
      cmd="$date +%s%N"
    fi
  fi

  eval "serve() { $cmd"'
}'
}

__main() {
  rm -f "$fifo"; mkfifo "$fifo"
  printf 1>&2 'fifo-srv "%s" "%s" started at %s
#touch "%s".quit  # to stop (needs final request)
' "$fifo" "$cmd" "$(serve)" "$fifo"
  trap "" PIPE
  while :; do
    serve >"$fifo" 2>/dev/null || :
    ! test -e "$fifo".quit || break
  done
  echo 'fifo-srv exiting at' "$(serve)"
  rm -f "$fifo" "$fifo".quit || :
}

__set_defaults
__main "$@"

# head -1 </tmp/nsecsrv-fifo &  # client # or cat, etc
# touch /tmp/nsecsrv-fifo.quit  # needs final request to exit
