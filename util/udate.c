#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <sys/time.h>

/*
 * strftime >stdout, but support %N (nanosec).
 * args: +FMT (like date)
 */


int printft( char *ftbuf, struct timeval tv, struct tm *tm ) {
  char *perc_N_pos = strstr( ftbuf, "%N" );
  if( NULL != perc_N_pos ) {
    perc_N_pos[0] = '\0';
    printf( "%s%06ld000", ftbuf, tv.tv_usec );
    printft( perc_N_pos + 2, tv, tm );
  } else {
    fputs( ftbuf, stdout );
  }
  return 0;
}

int main( int argc, char *argv[] ) {
  const char *fmt0 = "%s%N", *fmt = fmt0;
  char ftbuf [1024];
  struct timeval tv;
  struct tm *tm;

  if( 0 != gettimeofday( &tv, NULL ) ) return 1;
  if( argc <= 1 ) {
    printf( "%ld%06ld000\n", tv.tv_sec, tv.tv_usec );
    return 0;
  }
  fmt = argv [1];
  if( *fmt != '\0' && *fmt != '+' ) return 1;
  ++fmt;
  if( ! (tm = localtime( &tv.tv_sec )) ) return 1;
  if (0 == strftime( ftbuf, sizeof ftbuf, fmt, tm ) ) return 1;
  printft( ftbuf, tv, tm ); puts( "" );
}

/*
CFLAGS='-static -O3 -march=native -Wall' make udate; strip udate
./util/udate; date +%s%N
*/
