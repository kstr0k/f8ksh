#!/bin/sh
## or a lot faster: #!/my/busybox sh
## distro busyboxen don't provide nanodates, set F8KSH_FAKESH_DATE explicitly

test -n "${F8KSH_FAKESH_RC+SET}" ||  # unless: explicitly set (maybe '')
  ! test -r "${XDG_CONFIG_HOME:-$HOME/.config}/f8ksh/fakesh.rc" ||  # unless: rc missing
  F8KSH_FAKESH_RC="${XDG_CONFIG_HOME:-$HOME/.config}/f8ksh/fakesh.rc"
test -z "${F8KSH_FAKESH_RC:-}"  || . "$F8KSH_FAKESH_RC" --
test -z "${F8KSH_FAKESH_EVAL:-}" || eval "$F8KSH_FAKESH_EVAL"

# mess with this var b/c external cmd calls Heisenberg the measurement
# must be either code starting with 'now=...' or command that prints date
if test -z "${F8KSH_FAKESH_DATE:-}"; then
  if test -n "${EPOCHREALTIME:-}"; then
    F8KSH_FAKESH_DATE='now=$EPOCHREALTIME; now=${now%.*}${now#*.}; now=${now}000'
  else
    F8KSH_FAKESH_DATE='now=$(date -u +%s%N); case "$now" in
*N) now=${now%N}; now=${now%"%"}000000000 ;;
*) ;;
esac'
#??????????) now=${now}000000000 ;;  # bad date that strips %N # dont slow ^^
  fi
else
  case "$F8KSH_FAKESH_DATE" in
    'now='*) ;;
    *) F8KSH_FAKESH_DATE="now=\$($F8KSH_FAKESH_DATE)" ;;
  esac
fi
# most portable, yet parsable human date: '+%Y-%m-%dT%T+00:00'; also see +%s%N, -Ins; POSIX: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/date.html

__aux_e108dd7bb5d7af773a2() {
  case "${1:-}" in
    --log) ${F8KSH_FAKESH_DEBUG:-false} || return 0; shift; {
      local _fmt; _fmt=$1; shift
      printf 'log pid="%s" ppid="%s" '"$_fmt;\n" $$ $PPID "$@"
    } >>"${F8KSH_FAKESH_LOGDIR:-${TMPDIR:-/tmp}}"/fakesh.log
    ;;
    --log-start) shift
      local _arg _i _fmt; _i=1; _fmt='\\n  '
      for _arg; do
        _fmt="$_fmt arg_$_i="'"$(cat <<'\''e108dd7bb5d7af773a2_EOF'\''\n%s\ne108dd7bb5d7af773a2_EOF\n)"'
        _i=$(( _i + 1 ))
      done
      local now; eval "$F8KSH_FAKESH_DATE"
      __aux_e108dd7bb5d7af773a2 --log 'type="start" time="%s" \'"$_fmt" "$now" "$@"
      #__aux_e108dd7bb5d7af773a2 --log 'arg <<|%s|>>\n' "$@" "[ last=$# ]"
      ;;
    --log-end) shift
      local now; eval "$F8KSH_FAKESH_DATE"
      __aux_e108dd7bb5d7af773a2 --log 'time="%s" descr="%s" type="end"' "$now" "$1";
      ;;
  esac
}

! ${F8KSH_FAKESH_COUNT:-false} || {
  cntf="${F8KSH_FAKESH_LOGDIR:-${TMPDIR:-/tmp}}"/fakesh_cnt.log
  ! test -r "$cntf" || IFS= read -r cnt <"$cntf" || :
  test 2>/dev/null "$cnt" -ge 0 || cnt=0; echo $(( cnt + 1)) >"$cntf"
  unset cnt cntf
}

if test $# -lt 2 || test "$1" != '-c'; then  # pass to underlying shell
  IFS= read -r shebang <"$0"
  test "${shebang#'#!'}" != "$shebang" || shebang='#!/bin/sh'
  exec ${shebang#'#!'} "$@"
fi

__aux_e108dd7bb5d7af773a2 --log-start "$@"

# 1=-c
CMD_e108dd7bb5d7af773a2=$2  # can't unset, magic to avoid conflicts
shift 2
test $# -eq 0 || shift      # 3=self, rest=args

# execute
${F8KSH_FAKESH_PARSE_SHEBANG:-false} ||
  CMD_e108dd7bb5d7af773a2="  $CMD_e108dd7bb5d7af773a2"  # TODO: HACK
case "$CMD_e108dd7bb5d7af773a2" in
  '#!'*)
    {  # read shebang & body from $cmd in same process
      IFS= read -r shebang
      body=$(cat; echo X); body=${body%X}
    } <<EOF
$(echo "$CMD_e108dd7bb5d7af773a2")
EOF
    shebang=${shebang#??}
    prog=
    case "$body" in
    *[![:space:]]*)  # no whitespace languages, sorry
      prog=$(mktemp)
      printf '%s' "$body" >"$prog"
      set -- $shebang "$prog" "$@"
      ;;
    *)
      set -- exec $shebang /dev/null "$@"
      ;;
    esac
    __aux_e108dd7bb5d7af773a2 --log-end dispatch
    "$@"; f8kshrc=$?  # past "$@" may never be reached (exec) but so what
    rm -f "$prog"
    exit $f8kshrc
    ;;

  *)  # hand over to sh, fork or no fork
    if ${F8KSH_FAKESH_TIME_EVAL:-true}; then
      (eval "$CMD_e108dd7bb5d7af773a2"); f8kshrc=$?
      __aux_e108dd7bb5d7af773a2 --log-end eval
      exit $f8kshrc
    else
      __aux_e108dd7bb5d7af773a2 --log-end eval
      eval "$CMD_e108dd7bb5d7af773a2"
    fi
    ;;
esac
