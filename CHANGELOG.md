* v0.1.3 (unreleased)
  * fakesh:
    * `PARSE_SHEBANG` off by default
    * loads any `~/.config/f8ks/fakesh.rc` unless `F8KSH_FAKESH_RC=''`
    * fakesh.log timestamps:
      * default format changed to epoch nanoseconds
      * `F8KSH_FAKESH_DATE` can be `now=...` shell code
    * much faster fakesh-log-conv
  * setup-bbox:
    * --busybox=make compiles from source
    * FreeBSD support using linux binaries / brandelf; clang make seems broken
    * bootstrap busybox0 from alpine if none installed
  * new utils:
    * fifo-srv.sh (works as fast nanodate provider)
    * logcmdname (log command, automatically name logfile)

* v0.1.2
  * SQL:
    * `run2` table now normalized (`cmd_id` refers to `cmd` table)
    * `run` becomes a view, includes `tdelta`
    * add `cmd_stat` view
  * `setup-bbox` script (applet-biased busybox, gives fastest performance thus far)
  * add `--pp-*='...'` field preprocessors
  * add `F8KSH_FAKESH_RC`, `F8KSH_FAKESH_EVAL`
  * add util/udate
  * better docs

* v0.1.1 (unreleased)
  * [announcement](https://discuss.kakoune.com/t/profiling-sh-invocations-with-fakesh/1806)
  * add logging, counting, SQL generation

* v0.1.0 (unreleased)
  * [initial announcement](https://discuss.kakoune.com/t/shebangs-in-sh/1800)
