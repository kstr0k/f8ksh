package main

import (
	"fmt"
	"net"
	"os"
	"time"
)

const (
	CONN_TYPE = "tcp"
	CONN_HOST = "localhost"
	CONN_PORT = "8713"
)

func main() {
	l, err := net.Listen(CONN_TYPE, CONN_HOST+":"+CONN_PORT)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	defer l.Close()

	for {
		conn, err := l.Accept()
		if err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}
		go serve(conn)
	}
}

func serve(conn net.Conn) {
	buf := make([]byte, 8)
	t := time.Now()
	conn.Write([]byte(fmt.Sprintf("%d\n", t.UnixNano())))
	rlen, _ := conn.Read(buf)
	if rlen > 0 && buf[0] == 'q' {
		os.Exit(0)
	}
	conn.Close()
}

// # (cd util && go build nsec-srv.go && strip nsec-srv) && ./util/nsec-srv
// # (cd util && gccgo -o nsec-srv nsec-srv.go && strip nsec-srv) && ./util/nsec-srv
// see nsec-srv.py
