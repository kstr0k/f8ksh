#!/usr/bin/env python3
import datetime
from socketserver import BaseRequestHandler, TCPServer, UnixStreamServer
import tempfile

class Handler (BaseRequestHandler):
  def handle( self ):
    self.request.sendall( bytes( datetime.datetime.now().strftime("%s%f000\n"), 'ascii' ) )
    msg = self.request.recv( 128 )
    if len( msg ) > 1: raise Exception('done')

class TimeServer (object):
  def handle_error(self, *args):
    self.keepon = False
  def serve( self ):
    self.keepon = True
    while self.keepon:
      self.handle_request()

class TcpTimeServer (TimeServer, TCPServer):
  allow_reuse_address = True

class UnixStreamTimeServer (TimeServer, UnixStreamServer):
  allow_reuse_address = True

#with UnixStreamTimeServer( '/tmp/nsecsrv', handler ) as server:
with TcpTimeServer( ('', 8713), Handler ) as server:  # '[d]ateti[m]e' port
  server.serve()

# echo q | nc localhost 8713  # quit
# echo | nc localhost 8713
# echo | nc -U /tmp/nsecsrv
# head -1 < /dev/tcp/localhost/8713  # bash
# nc -q 0 ...  # may not be available
# nc <...  # dummy file to avoid pipe but send single byte (don't hang)
# # hyperfine -S sh, -S busybox-sh, -S bash (<<EOF may still slow down)
